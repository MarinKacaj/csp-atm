package com.marin.csp.atm.business;

import com.marin.csp.atm.ui.cardslot.CardSlotPeripheral;
import com.marin.csp.atm.ui.cardslot.CardSlotPeripheralEvent;
import com.marin.csp.atm.ui.cardslot.CardSlotPeripheralState;
import org.jcsp.lang.CSProcess;
import org.jcsp.lang.ChannelInput;
import org.jcsp.lang.ChannelOutput;

public class CardSlotManager implements CSProcess {

    private final ChannelInput<CardSlotPeripheralEvent> requestedTransitionInput;
    private final ChannelOutput<String> cardSerialNumberOutput;
    private final CardSlotPeripheral cardSlotPeripheral;

    public CardSlotManager(ChannelInput<CardSlotPeripheralEvent> requestedTransitionInput,
                           ChannelOutput<String> cardSerialNumberOutput,
                           CardSlotPeripheral cardSlotPeripheral) {
        this.requestedTransitionInput = requestedTransitionInput;
        this.cardSerialNumberOutput = cardSerialNumberOutput;
        this.cardSlotPeripheral = cardSlotPeripheral;
    }

    @Override
    public void run() {
        CardSlotPeripheralState state = new CardSlotPeripheralState.NoCard();
        while (true) {
            CardSlotPeripheralEvent requestedTransition = requestedTransitionInput.read();
            state = reduce(state, requestedTransition);
        }
    }

    private CardSlotPeripheralState reduce(final CardSlotPeripheralState currentState,
                                           final CardSlotPeripheralEvent requestedTransition) {
        return currentState.match(
                noCard -> requestedTransition.match(
                        cardInserted -> new CardSlotPeripheralState.CardInside(),
                        serialNumRequested -> currentState,
                        cardEjectionRequested -> currentState),
                cardInside -> requestedTransition.match(
                        cardInserted -> currentState,
                        serialNumRequested -> {
                            String serialNum = cardSlotPeripheral.readSerialNum();
                            cardSerialNumberOutput.write(serialNum);
                            return new CardSlotPeripheralState.CardInside();
                        },
                        cardEjectionRequested -> {
                            cardSlotPeripheral.eject();
                            return new CardSlotPeripheralState.NoCard();
                        })
        );
    }
}
