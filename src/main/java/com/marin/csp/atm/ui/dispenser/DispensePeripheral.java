package com.marin.csp.atm.ui.dispenser;

public interface DispensePeripheral {

    void act(DispensePeripheralState.Idle state);

    void act(DispensePeripheralState.Dispense state);
}
