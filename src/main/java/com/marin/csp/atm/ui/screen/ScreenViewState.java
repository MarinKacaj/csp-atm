package com.marin.csp.atm.ui.screen;

public interface ScreenViewState {

    void into(ScreenView screenView);

    final class NoMoneyError implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class Initial implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class InvalidCardError implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class PasswordPrompt implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class TooManyWrongPasswordsError implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class PasswordVerificationProgress implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class BadPasswordError implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class BadBankCodeError implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class BadAccountError implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class WithdrawalTransactionProgress implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class WithdrawalSumPrompt implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class CustomerShouldTakeCardError implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }

    final class CustomerShouldTakeCardSuccess implements ScreenViewState {

        @Override
        public void into(ScreenView screenView) {
            screenView.render(this);
        }
    }
}
