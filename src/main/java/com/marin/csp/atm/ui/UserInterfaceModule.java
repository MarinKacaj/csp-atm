package com.marin.csp.atm.ui;

import com.marin.csp.atm.ui.cardslot.CardSlot;
import com.marin.csp.atm.ui.cardslot.CardSlotPeripheral;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class UserInterfaceModule {

    @Provides
    @Singleton
    CardSlotPeripheral provideCardSlotPeripheral() {
        return new CardSlot();
    }
}
