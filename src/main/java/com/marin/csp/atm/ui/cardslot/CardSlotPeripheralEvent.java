package com.marin.csp.atm.ui.cardslot;

import java.util.function.Function;

public interface CardSlotPeripheralEvent {

    <T> T match(Function<CardInserted, T> cardInserted,
                Function<SerialNumRequested, T> serialNumRequested,
                Function<CardEjectionRequested, T> cardEjectionRequested);

    final class CardInserted implements CardSlotPeripheralEvent {

        @Override
        public <T> T match(Function<CardInserted, T> cardInserted,
                           Function<SerialNumRequested, T> serialNumRequested,
                           Function<CardEjectionRequested, T> cardEjectionRequested) {
            return cardInserted.apply(this);
        }
    }

    final class SerialNumRequested implements CardSlotPeripheralEvent {

        @Override
        public <T> T match(Function<CardInserted, T> cardInserted,
                           Function<SerialNumRequested, T> serialNumRequested,
                           Function<CardEjectionRequested, T> cardEjectionRequested) {
            return serialNumRequested.apply(this);
        }
    }

    final class CardEjectionRequested implements CardSlotPeripheralEvent {

        @Override
        public <T> T match(Function<CardInserted, T> cardInserted,
                           Function<SerialNumRequested, T> serialNumRequested,
                           Function<CardEjectionRequested, T> cardEjectionRequested) {
            return cardEjectionRequested.apply(this);
        }
    }
}
