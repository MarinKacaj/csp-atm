package com.marin.csp.atm.ui.cardslot;

public interface CardSlotPeripheral {

    void eject();

    String readSerialNum();
}
