package com.marin.csp.atm.ui.cardslot;

import java.util.function.Function;

public interface CardSlotPeripheralState {

    <T> T match(Function<NoCard, T> noCard, Function<CardInside, T> cardInside);

    final class NoCard implements CardSlotPeripheralState {

        @Override
        public <T> T match(Function<NoCard, T> noCard,
                           Function<CardInside, T> cardInside) {
            return noCard.apply(this);
        }
    }

    final class CardInside implements CardSlotPeripheralState {

        @Override
        public <T> T match(Function<NoCard, T> noCard,
                           Function<CardInside, T> cardInside) {
            return cardInside.apply(this);
        }
    }
}
