package com.marin.csp.atm.ui.dispenser;

public interface DispensePeripheralState {

    final class Dispense implements DispensePeripheralState {

        private int sum;

        public Dispense(int sum) {
            this.sum = sum;
        }

        public int getSum() {
            return sum;
        }
    }

    final class Idle implements DispensePeripheralState {
    }
}
