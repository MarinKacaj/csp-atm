package com.marin.csp.atm.ui.screen;

public class Screen implements ScreenView {

    @Override
    public void render(ScreenViewState.NoMoneyError state) {
    }

    @Override
    public void render(ScreenViewState.Initial state) {
    }

    @Override
    public void render(ScreenViewState.InvalidCardError state) {
    }

    @Override
    public void render(ScreenViewState.PasswordPrompt state) {
    }

    @Override
    public void render(ScreenViewState.TooManyWrongPasswordsError state) {
    }

    @Override
    public void render(ScreenViewState.PasswordVerificationProgress state) {
    }

    @Override
    public void render(ScreenViewState.BadPasswordError state) {
    }

    @Override
    public void render(ScreenViewState.BadBankCodeError state) {
    }

    @Override
    public void render(ScreenViewState.BadAccountError state) {
    }

    @Override
    public void render(ScreenViewState.WithdrawalTransactionProgress state) {
    }

    @Override
    public void render(ScreenViewState.WithdrawalSumPrompt state) {
    }

    @Override
    public void render(ScreenViewState.CustomerShouldTakeCardError state) {
    }

    @Override
    public void render(ScreenViewState.CustomerShouldTakeCardSuccess state) {
    }
}
