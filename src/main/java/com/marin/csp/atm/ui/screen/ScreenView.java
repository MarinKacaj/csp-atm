package com.marin.csp.atm.ui.screen;

public interface ScreenView {

    void render(ScreenViewState.NoMoneyError state);

    void render(ScreenViewState.Initial state);

    void render(ScreenViewState.InvalidCardError state);

    void render(ScreenViewState.PasswordPrompt state);

    void render(ScreenViewState.TooManyWrongPasswordsError state);

    void render(ScreenViewState.PasswordVerificationProgress state);

    void render(ScreenViewState.BadPasswordError state);

    void render(ScreenViewState.BadBankCodeError state);

    void render(ScreenViewState.BadAccountError state);

    void render(ScreenViewState.WithdrawalTransactionProgress state);

    void render(ScreenViewState.WithdrawalSumPrompt state);

    void render(ScreenViewState.CustomerShouldTakeCardError state);

    void render(ScreenViewState.CustomerShouldTakeCardSuccess state);
}
